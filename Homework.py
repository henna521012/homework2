import json
from ExampleData import department_table    # 系所代號對照資料Dictionary
from ExampleData import route_table # 入學管道代號對照資料Dictionary


def HW2(text, user_id): # text => 接收到的訊息字串;   user_id => User的Line ID

    student_id = text.split('/')[1][3:12]
    student_data = {student_id:dict(name=text.split('/')[0][5:], user_id=user_id)}  # 學生資料
    department = text.split('/')[1][3:5]   # 學號對應的系所
    route = text.split('/')[1][8:9]  # 入學管道

    ### 請在下方完成作業功能的程式碼 ###

    str = department_table.get(department)
    str2 = route_table.get(route)

    if str is None:
        return ('找不到這個系所!', student_data)
    else:
        return ('您的系所是 ' + str + ' ，入學管道是 ' + str2, student_data)